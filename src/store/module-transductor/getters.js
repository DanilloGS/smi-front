const openMap = (state) => {
  return state.openMap
}
const filterOptions = (state) => {
  return state.filterOptions
}
const chartOptions = (state) => {
  return state.chartOptions
}

export {
  openMap,
  filterOptions,
  chartOptions
}
